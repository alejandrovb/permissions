**WEB API**

Web API for manage user permissions.

**INSTALL**

***************************************************************************************************************************
**Web API**

In Users.Permissions.WebAPI directory open terminal and execute:


- dotnet restore
- dotnet publish -c Release -o out
- docker build -t  webapicore .
- docker run -d -p 5024:5024 --name webapipermissions webapicore


NOTES:
- Set ConnectionStrings and ESConfiguration in appsettings.json 

***************************************************************************************************************************
**Database**

Open terminal and execute:

docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=P3rm1ss10n" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2022-latest

Connect to dabase with your IDE and exexute permissions\Scripts\Database\Create_data_base_user_permissions.sql

NOTES: 
- If is necesary, create inbound rule for enable port 1433 with tcp protocol


***************************************************************************************************************************
**Elasticshearch**

Open terminal and execute:

docker network create elastic
docker run --name es01 --net elastic -p 9200:9200 -it docker.elastic.co/elasticsearch/elasticsearch:8.7.0
docker cp es01:/usr/share/elasticsearch/config/certs/http_ca.crt .
curl --cacert http_ca.crt -u elastic https://localhost:9200

NOTES: 
- If get error message "CERT_TRUST_REVOCATION_STATUS_UNKNOWN" on Window, considering add --ssl-revoke-best-effort
- If docker container doesn't start considering execute:
		1. wsl -d docker-desktop (on windows)
		2. sysctl -w vm.max_map_count=262144 
***************************************************************************************************************************
**TEST**

**REST API**

**Get list of permissions**

Request

permissions/GetPermissions

curl --location 'http://localhost:5024/permissions/GetPermissions'

Response

{
        "id": 1,
        "employeeForename": "Alejandro",
        "employeeSurname": "Vargas",
        "permissionType": {
            "id": 2,
            "description": "Read"
        },
        "permissionDate": "2023-04-11T01:34:29.537"
    },
    {
        "id": 2,
        "employeeForename": "Luis Alejandro",
        "employeeSurname": "Vargas Bolivar",
        "permissionType": {
            "id": 1,
            "description": "Create"
        },
        "permissionDate": "2023-04-11T02:06:25.917"
    }

**Create new permission**

Request

permissions/RequestPermission

curl --location 'http://localhost:5024/permissions/RequestPermission' \
--header 'Content-Type: application/json' \
--data '{
	"employeeForename": "Luis Alejandro",
	"employeeSurname": "Vargas Bolivar",
	"PermissionType": {
		"Id": "1"
	}
}'

Response

{
    "id": 6,
    "employeeForename": "Luis Alejandro",
    "employeeSurname": "Vargas Bolivar",
    "permissionType": {
        "id": 1
    },
    "permissionDate": "2023-04-11T13:06:25.2070901-05:00"
}

**Modify new permission**

Request

permissions/ModifyPermission

curl --location 'http://localhost:5024/permissions/ModifyPermission' \
--header 'Content-Type: application/json' \
--data '{	
    "id":1,
    "employeeForename": "Alejandro",
	"employeeSurname": "Vargas",
	"PermissionType": {
		"Id": "2"
	}
}'

Response

{
    "id": 1,
    "employeeForename": "Alejandro",
    "employeeSurname": "Vargas",
    "permissionType": {
        "id": 2
    },
    "permissionDate": "0001-01-01T00:00:00"
}





