﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Users.Permissions.Domain
{
    public class PermissionDto
    {
        public int Id { get; set; } 
        public string EmployeeForename { get; set; }
        public string EmployeeSurname { get; set; }        
        public PermissionTypeDto PermissionType { get; set; }
        public DateTime PermissionDate { get; set; }

    }
}
