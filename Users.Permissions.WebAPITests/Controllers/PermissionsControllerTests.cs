﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Users.Permissions.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Users.Permissions.Infraestructure.Models;
using Users.Permissions.WebAPI.Application.Commands;
using MediatR;
using Users.Permissions.Domain;
using System.Security;
using Nest;

namespace Users.Permissions.WebAPI.Controllers.Tests
{
    [TestClass()]
    public class PermissionsControllerTests
    {
        private readonly IMediator _mediator;
        private readonly IElasticClient _elasticClient;

        public PermissionsControllerTests(IMediator mediator, IElasticClient elasticClient)
        {
            _mediator = mediator;
            _elasticClient = elasticClient;
        }

        [TestMethod()]
        public void CreateTest()
        {

            PermissionDto permission = new PermissionDto()
            {
                EmployeeForename = "Alejando",
                EmployeeSurname = "Vargas",
                PermissionType = new PermissionTypeDto() { Id = 3 }
            };
            
            var controller = new PermissionsController(_mediator, _elasticClient);

            // Act
            Task<ActionResult<PermissionDto>> newPermission = controller.Create(permission);

            // Assert
            Assert.IsInstanceOfType(newPermission, typeof(OkResult));
        }

        [TestMethod()]
        public void EditTest()
        {
            PermissionDto permission = new PermissionDto()
            {
                Id = 1,
                EmployeeForename = "Alejando",
                EmployeeSurname = "Vargas",
                PermissionType = new PermissionTypeDto() { Id = 1 }
            };

            var controller = new PermissionsController(_mediator, _elasticClient);

            //Act
            Task<ActionResult<PermissionDto>> currentPermission = controller.Edit(permission);

            // Assert
            Assert.IsInstanceOfType(currentPermission, typeof(OkResult));
        }

        [TestMethod()]
        public void GetPermissionsAsyncTest()
        {
            var controller = new PermissionsController(_mediator, _elasticClient);

            // Act
            Task<ActionResult<List<PermissionDto>>> currentPermission = controller.GetPermissions();

            // Assert
            Assert.IsInstanceOfType(currentPermission, typeof(OkResult));
        }
    }
}