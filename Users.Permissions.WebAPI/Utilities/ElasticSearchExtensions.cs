﻿using Nest;
using Users.Permissions.Domain;

namespace Users.Permissions.WebAPI.Utilities
{
    public static class ElasticSearchExtensions
    {
        public static void AddElasticsearch(
           this IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["ESConfiguration:Uri"];
            var defaultIndex = configuration["ESConfiguration:index"];
            var user = configuration["ESConfiguration:user"];
            var password = configuration["ESConfiguration:password"];

            var settings = new ConnectionSettings(new Uri(url)).BasicAuthentication(user, password)
                .PrettyJson()
                .DefaultIndex(defaultIndex);

            AddDefaultMappings(settings);

            var client = new ElasticClient(settings);

            services.AddSingleton<IElasticClient>(client);

            CreateIndex(client, defaultIndex);
        }

        private static void AddDefaultMappings(ConnectionSettings settings)
        {
            settings
                .DefaultMappingFor<PermissionDto>(m => m
                    .Ignore(p => p.PermissionType)
                );
        }

        private static void CreateIndex(IElasticClient client, string indexName)
        {
            var createIndexResponse = client.Indices.Create(indexName,
                index => index.Map<PermissionDto>(x => x.AutoMap())
            );
        }
    }
}
