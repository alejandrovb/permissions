﻿using MediatR;
using Users.Permissions.Domain;

namespace Users.Permissions.WebAPI.Application.Queries
{
    public class PermissionQueries : IRequest<List<PermissionDto>>
    {
    }    
}
