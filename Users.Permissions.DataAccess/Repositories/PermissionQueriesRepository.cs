﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Permissions.Domain;
using Users.Permissions.Infraestructure.Models;

namespace Users.Permissions.Infraestructure.Repositories
{
    public class PermissionQueriesRepository: IPermissionQueriesRepository
    {
        private readonly UserPermissionContext _context;

        public PermissionQueriesRepository(UserPermissionContext context)
        {
            _context = context;
        }

        /// <summary>
        ///Get existing permissions 
        /// </summary>
        /// <returns>Permissions list</returns>
        public async Task<List<PermissionDto>> GetPermissions() 
        {
            List<Permission> permissions = await _context.Permissions.Include(x => x.PermissionTypeNavigation).ToListAsync();

            if (permissions != null && permissions.Count > 0)
            {
                return permissions.ConvertAll(item => new PermissionDto()
                {
                    Id = item.Id,
                    EmployeeForename = item.EmployeeForename,
                    EmployeeSurname = item.EmployeeSurname,
                    PermissionType = new PermissionTypeDto() 
                    { 
                        Id = item.PermissionType,
                        Description = item.PermissionTypeNavigation.Description
                    },
                    PermissionDate = item.PermissionDate
                }).ToList();
            }
            else
                return new List<PermissionDto>();
        }
    }
}
