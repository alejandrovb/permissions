﻿using Users.Permissions.Domain;
using Microsoft.EntityFrameworkCore.Internal;
using Users.Permissions.Infraestructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Users.Permissions.Infraestructure.Repositories
{
    public class PermissionCommandsRepository: IPermissionCommandsRepository
    {
        private readonly UserPermissionContext _context;

        public PermissionCommandsRepository(UserPermissionContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create permission
        /// </summary>
        /// <param name="permission">Permission object</param>
        public async Task<PermissionDto> SavePermission(PermissionDto permission) 
        {
            Permission newPermission = new Permission()
            { 
                EmployeeForename = permission.EmployeeForename,
                EmployeeSurname = permission.EmployeeSurname,
                PermissionType = permission.PermissionType.Id,
                PermissionDate = DateTime.Now
            };

            _context.Permissions.Add(newPermission);
            await _context.SaveChangesAsync();
            permission.Id = newPermission.Id;
            permission.PermissionDate = newPermission.PermissionDate;

            return permission;
        }

        /// <summary>
        /// Edit permission
        /// </summary>
        /// <param name="permission">Permission object</param>
        /// <returns>Permission object</returns>
        public async Task<PermissionDto> EditPermission(PermissionDto permission) 
        {
            Permission permisionEntity = await _context.Permissions.FirstOrDefaultAsync(x => x.Id == permission.Id);

            if (permisionEntity != null)
            {
                permisionEntity.EmployeeForename = permission.EmployeeForename;
                permisionEntity.EmployeeSurname = permission.EmployeeSurname;
                permisionEntity.PermissionType = permission.PermissionType.Id;
                permisionEntity.PermissionDate = DateTime.Now;

                await _context.SaveChangesAsync();
            }

            return permission;
        }
    }
}
