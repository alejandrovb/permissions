﻿using MediatR;
using Users.Permissions.Domain;
using Users.Permissions.Infraestructure.Repositories;
using Users.Permissions.WebAPI.Application.Commands;

namespace Users.Permissions.WebAPI.Application.DomainEventHandlers
{
    public class PersistPermissionHandler: IRequestHandler<PermissionCommands, PermissionDto>
    {
        private readonly IPermissionCommandsRepository _repository;

        public PersistPermissionHandler(IPermissionCommandsRepository repository)
        {
            _repository = repository;  
        }

        public async Task<PermissionDto> Handle(PermissionCommands command, CancellationToken cancellationToken)
        {
            PermissionDto permission = new PermissionDto()
            {                
                EmployeeForename = command.EmployeeForename,
                EmployeeSurname = command.EmployeeSurname,
                PermissionType = new PermissionTypeDto() { Id = command.PermissionType.Id }
            };

            if (command.Id == 0)
                return await _repository.SavePermission(permission);
            else
            {
                permission.Id = command.Id;

                return await _repository.EditPermission(permission);
            }
        }
    }
}
