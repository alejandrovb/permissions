﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.AspNetCore.Mvc;
using Nest;
using Serilog;
using System.Net;
using Users.Permissions.Domain;
using Users.Permissions.WebAPI.Application.Commands;
using Users.Permissions.WebAPI.Application.Queries;
using Users.Permissions.WebAPI.Application.Utilities;
using static Users.Permissions.WebAPI.Application.Utilities.Enum;

namespace Users.Permissions.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]

    public class PermissionsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IElasticClient _elasticClient;


        public PermissionsController(IMediator mediator, IElasticClient elasticClient)
        {
            _mediator = mediator;            
            _elasticClient = elasticClient;
        }

        /// <summary>
        /// Create permission
        /// </summary>
        /// <param name="permission">permission object</param>
        /// <returns>Permission object</returns>
        [HttpPost("RequestPermission")]
        public async Task<ActionResult<PermissionDto>> Create(PermissionDto permission)
        {
            try
            {
                if (permission == null || string.IsNullOrEmpty(permission.EmployeeForename) || string.IsNullOrEmpty(permission.EmployeeSurname) || permission.PermissionType.Id == 0)
                {
                    Log(LogTypes.Warning.ToString(), Message.WARNING_BAD_REQUEST);

                    return BadRequest(Message.WARNING_BAD_REQUEST);
                }
                else
                {
                    var newPermission = await _mediator.Send(new PermissionCommands
                        (
                        0,
                        permission.EmployeeForename,
                        permission.EmployeeSurname,
                        permission.PermissionType.Id
                        ));

                    Log(LogTypes.Information.ToString(), Message.INFORMATION_CREATE_SUCCESS);

                    await _elasticClient.IndexDocumentAsync(newPermission);

                    return Ok(newPermission);
                }
            }
            catch (Exception ex)
            {                
                Log(LogTypes.Error.ToString(), string.Concat(Message.EXCEPTION,": ", ex.InnerException.Message));

                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    httpCode = StatusCodes.Status500InternalServerError,
                    message = ex.InnerException.Message
                });


            }

        }

        /// <summary>
        /// Create permission
        /// </summary>
        /// <param name="permission">permission object</param>
        /// <returns>Permission object</returns>
        [HttpPost("ModifyPermission")]
        public async Task<ActionResult<PermissionDto>> Edit(PermissionDto permission)
        {
            try
            {
                if (permission == null || permission.Id == 0 || string.IsNullOrEmpty(permission.EmployeeForename) || string.IsNullOrEmpty(permission.EmployeeSurname) || permission.PermissionType.Id == 0)
                {
                    Log(LogTypes.Warning.ToString(), Message.WARNING_BAD_REQUEST);

                    return BadRequest(Message.WARNING_BAD_REQUEST);
                }
                else
                {
                    var currentPermission = await _mediator.Send(new PermissionCommands
                        (
                        permission.Id,
                        permission.EmployeeForename,
                        permission.EmployeeSurname,
                        permission.PermissionType.Id
                        ));
                    
                    Log(LogTypes.Information.ToString(), Message.INFORMATION_EDIT_SUCCESS);

                    return Ok(currentPermission);
                }
            }
            catch (Exception ex)
            {
                Log(LogTypes.Error.ToString(), string.Concat(Message.EXCEPTION, ": ", ex.InnerException.Message));

                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    httpCode = StatusCodes.Status500InternalServerError,
                    message = ex.InnerException.Message
                });
            }
        }

        /// <summary>
        /// Get permissions list
        /// </summary>
        /// <returns>Permissions list</returns> 
        [HttpGet("GetPermissions")]
        public async Task<ActionResult<List<PermissionDto>>> GetPermissions()
        {
            try
            {
                var permissions = await _mediator.Send(new PermissionQueries());

                Log(LogTypes.Information.ToString(), Message.INFORMATION_GET_SUCCESS);
                
                return permissions;
            }
            catch (Exception ex)
            {
                Log(LogTypes.Error.ToString(), string.Concat(Message.EXCEPTION, ": ", ex.InnerException.Message));

                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    httpCode = StatusCodes.Status500InternalServerError,
                    message = ex.InnerException.Message
                });
            }
            
        }

        /// <summary>
        /// Write log
        /// </summary>
        /// <param name="type">log type</param>
        /// <param name="message">log message</param>
        private void Log(string type, string message)
        {
            using (var log = new LoggerConfiguration().WriteTo.Console().CreateLogger())
            {
                if(type == LogTypes.Information.ToString())
                    log.Information(String.Format("Information: {0}",message));
                else if (type == LogTypes.Warning.ToString())
                    log.Warning(String.Format("Warning: {0}", message));
                else if (type == LogTypes.Error.ToString())
                    log.Error(String.Format("Error: {0}", message));
            }
        }
    }
}
