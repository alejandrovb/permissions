﻿namespace Users.Permissions.WebAPI.Application.Utilities
{
    public class Message
    {
        public const string INFORMATION_CREATE_SUCCESS = "Permiso creado correctamente.";
        public const string INFORMATION_EDIT_SUCCESS = "Permiso editado correctamente.";
        public const string INFORMATION_GET_SUCCESS = "Permisos obtenidos correctamente.";
        public const string WARNING_BAD_REQUEST = "Los valores de entrada no tienen el formato correcto.";
        public const string EXCEPTION = "Error interno";
    }
}
