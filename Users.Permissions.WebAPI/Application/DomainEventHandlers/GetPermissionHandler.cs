﻿using MediatR;
using Users.Permissions.Domain;
using Users.Permissions.Infraestructure.Repositories;
using Users.Permissions.WebAPI.Application.Queries;

namespace Users.Permissions.WebAPI.Application.DomainEventHandlers
{
    public class GetPermissionHandler: IRequestHandler<PermissionQueries, List<PermissionDto>>
    {
        private readonly IPermissionQueriesRepository _repository;

        public GetPermissionHandler(IPermissionQueriesRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<PermissionDto>> Handle(PermissionQueries query, CancellationToken cancellationToken)
        {
            return await _repository.GetPermissions();
        }
    }
}
