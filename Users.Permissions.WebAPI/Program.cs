using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Users.Permissions.Infraestructure.Models;
using Users.Permissions.Infraestructure.Repositories;
using Users.Permissions.WebAPI.Utilities;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
builder.Services.AddDbContext<UserPermissionContext>();
builder.Services.AddScoped<IPermissionCommandsRepository, PermissionCommandsRepository>();
builder.Services.AddScoped<IPermissionQueriesRepository, PermissionQueriesRepository>();
builder.Services.AddElasticsearch(builder.Configuration);
builder.Services.AddControllers();

//builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

//Configure the HTTP request pipeline.

//app.UseHttpsRedirection();

//app.UseAuthorization();

app.MapControllers();

app.Run("http://*:5024");
