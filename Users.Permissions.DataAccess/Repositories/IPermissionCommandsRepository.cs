﻿using Users.Permissions.Domain;

namespace Users.Permissions.Infraestructure.Repositories
{
    public interface IPermissionCommandsRepository
    {
        Task<PermissionDto> SavePermission(PermissionDto permission);

        Task<PermissionDto> EditPermission(PermissionDto permission);
    }
}
