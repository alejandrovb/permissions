﻿using Users.Permissions.Domain;
using MediatR;
using Users.Permissions.Infraestructure.Repositories;

namespace Users.Permissions.WebAPI.Application.Commands
{
    public class PermissionCommands : IRequest<PermissionDto>
    {
        public int Id { get; set; }
        public string EmployeeForename { get; set; }
        public string EmployeeSurname { get; set; }
        public PermissionTypeDto PermissionType { get; set; }
        

        public PermissionCommands(int id, string employeeForename, string employeeSurname, int idPermissionType)
        {
            Id = id;
            EmployeeForename = employeeForename;
            EmployeeSurname = employeeSurname;
            PermissionType = new PermissionTypeDto() { Id = idPermissionType, Description = string.Empty };
        }

    }
}
