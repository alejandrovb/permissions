﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Permissions.Domain;

namespace Users.Permissions.Infraestructure.Repositories
{
    public interface IPermissionQueriesRepository
    {
        Task<List<PermissionDto>> GetPermissions();
    }
}
